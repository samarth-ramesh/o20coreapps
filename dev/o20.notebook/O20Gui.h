#ifndef WORDINTERFACE_H
#define WORDINTERFACE_H

#include <QMainWindow>
#include <QSpinBox>

#include <Sonnet/SpellCheckDecorator>

QT_BEGIN_NAMESPACE
namespace Ui { class O20Gui; }
QT_END_NAMESPACE

class O20Gui : public QMainWindow
{
    Q_OBJECT

public:
    O20Gui(QWidget *parent = nullptr);
    ~O20Gui();

    void startCore() {}

/*private slots:
    void createnewnotepage();
    void renamenote();
    void savenotes();
    void opennotes();
    void changenoterow();

    void showContextMenu(const QPoint &pos);

protected:
    void closeEvent(QCloseEvent* event);
*/

private:
    Ui::O20Gui *ui;

    /*
    QString notebooksfolder;
    QString oldname;
    QVector<QString> notepages;
    QVector<QString> notepagesnames;

    int row;
    */
};

#endif // WORDINTERFACE_H
