#include "O20Gui.h"
#include "src/ui_O20Ui.h"
#include <converter.h>

#include <QTest>
#include <QQuickItem>
#include <QQuickView>
#include <QtWidgets>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

#include <KCharSelect>
#include <O20>

/*
 Initialize O20.Word
 */

void O20Gui::enableEditing() {
    if (!plaintextmode) // disable ribbon
        for (int index = 0; index < ui->ribbon->count(); ++index)
            ui->ribbon->widget(index)->setEnabled(true);

    ui->pageStack->setEnabled(true);
    ui->statusbar->setEnabled(true);
    ui->windec->setEnabled(true);
    ui->helphome->show();
    ui->navButton->show();
}

#define HOMEDIR QStandardPaths::writableLocation(QStandardPaths::HomeLocation)

QObject* myObject;
QThread* threadt;

O20Gui::O20Gui(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::O20Gui)
    , tableh(new QSpinBox)
    , tablew(new QSpinBox)
    , url(new QLineEdit)
    , urlname(new QLineEdit)
{

	// setup user interface
    ui->setupUi(this);
    nostart = false;
    setWindowTitle();

    ribbonminsize = ui->ribbon->tabBar()->size().height();

    //ui->startAbout->hide();
    //ui->About->hide();
    ui->headingList_2->clear();

    for (int index = 0; index < ui->ribbon->count(); ++index) ui->ribbon->widget(index)->setEnabled(false);
    ui->pageStack->setEnabled(false);
    ui->statusbar->setEnabled(false);
    ui->windec->setEnabled(false);
    ui->helphome->hide();
    ui->navButton->hide();

    charSelectDialog = new QDialog(this);
    charSelectDialog->resize(500, 250);

    KCharSelect* insertChar = new KCharSelect(charSelectDialog, nullptr, KCharSelect::SearchLine | KCharSelect::CharacterTable |
		    KCharSelect::DetailBrowser | KCharSelect::BlockCombos);

    charSelectDialog->setWindowTitle(tr("O20 Special Characters"));
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(insertChar);
    layout->setContentsMargins(5,5,5,5);
    charSelectDialog->setLayout(layout);

    if (!qEnvironmentVariableIsSet("SNAP"))
        LoadFileManager(HOMEDIR);
    else
        LoadFileManager(HOMEDIR + "/../.."); // HOME/snap/office20/ --> HOME/snap/office20/.. --> HOME

    connect(ui->fileManager, &QListWidget::itemDoubleClicked, [=]() {
            int row = ui->fileManager->currentRow();
            QFileInfo fileInfo = list.at(row);
            if (fileInfo.isDir()) LoadFileManager(fileInfo.absoluteFilePath());
            else ioReceiver("open:", fileInfo.filePath(), ui->openLocked->isChecked());
    });

    //if (!qEnvironmentVariableIsSet("SNAP")) ui->snapRefresh->hide();
    //ui->abouto20->setText(QString(tr("About O20 %1")).arg(O20_VERSION_STR));
    //ui->qversion->setText(QString(tr("Built with Qt %1")).arg(QT_VERSION_STR));

    ui->assistiveMode->hide();
    ui->editsidebar->setCurrentIndex(1);
    ui->searchTabbed->setCurrentIndex(0);
    ui->quickDock->hide();

    ui->notification->hide();
    ui->notificationerror->hide();
    ui->pageStack->setCurrentIndex(0);

    //ui->finder->show();
    //ui->finder->setWindowFlags(Qt::Widget);    
    //ui->replacer->show();
    //ui->replacer->setWindowFlags(Qt::Widget);

    ui->recentFiles->clear();
    LoadRecentFiles();

    /*ui->spectrumSearch->hide();
    ui->furtherInfo->hide();*/

    ui->widget_4->hide();
    ui->recentFiles->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->recentFiles, SIGNAL(customContextMenuRequested(const QPoint &)),
        this, SLOT(ProvideContextMenu(const QPoint &)));

    ui->pageview->setChecked(true);
    QRect ooo = ui->spectrumSearchInput->geometry();
    ooo.moveBottom(ui->spectrumSearchInput->pos().y()+150);
    ui->spectrumSearchInput->setGeometry(ooo);

    ui->saveButton->setHidden(true);
    connect(ui->switch_2->rootObject()->findChild<QObject*>("Switch"), SIGNAL(released()), SLOT(UpdateAutosave()));

    //wordtray = new QSystemTrayIcon(QIcon(":/icons/app/ms-onedrive.svg.png"), this);
    //wordtray->setToolTip("Autosaving Documents...");
    //wordtray->setVisible(false);

    {

        QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect;
        effect->setBlurRadius(10);
        effect->setXOffset(5);
        effect->setYOffset(5);
        effect->setColor(QBrush("#e5e4e3").color());

        ui->recentFiles->setGraphicsEffect(effect);
    }

    {

        QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect;
        effect->setBlurRadius(10);
        effect->setXOffset(2);
        effect->setYOffset(3);
        effect->setColor(QBrush("#e5e4e3").color());

        ui->topbar->setGraphicsEffect(effect);
    }

    for (QWidget* w : {ui->blankfromtemplate, ui->openstart, ui->openLocked, ui->openAsText,
		    ui->aboutQt, ui->abstractsoftware, ui->reportBug, ui->submitIdea,
		    ui->o20_word, ui->o20_notebook, ui->o20_slideshow}) {
        QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect;
        effect->setBlurRadius(10);
        effect->setXOffset(5);
        effect->setYOffset(5);
        effect->setColor(QBrush("#e5e4e3").color());

        w->setGraphicsEffect(effect);
        //ui->startsave->setGraphicsEffect(effect);
    }

    for (QWidget* w : {/*ui->startsave,*/ ui->openButton, ui->openTextFileButton, ui->openfilelocation,
		       ui->save, ui->saveAs, ui->saveAsPDF, ui->print, ui->exportHtml, ui->exportText,
		       ui->renameDocument, ui->deleteDocument}) {
        QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect;
        effect->setBlurRadius(10);
        effect->setXOffset(5);
        effect->setYOffset(5);
        effect->setColor(QBrush("#e5e4e3").color());

        w->setGraphicsEffect(effect);
        //ui->startsave->setGraphicsEffect(effect);
    }

    //QMovie *ss = new QMovie(":/CortanaAnimationJa.gif");
    //ui->spectrumanimation->setMovie(ss);
    //ss->start();
    //ui->spectrumanimation->hide();

    // hide some disabled features
    //ui->startsave->hide();
    ui->setOverline->hide();
    //ui->autosavealert->hide();
    //ui->readonlyalert->hide();
    ui->openfilelocation->hide();

    #ifdef Q_OS_DARWIN
/*    QFile File(":/style/ribbon/RibbonStyle.mac.css");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());
    ui->ribbon->setStyleSheet(StyleSheet);
    ui->clipboard->setAutoRaise(false);
    ui->bulletedlist->setAutoRaise(false);
    ui->setColor->setAutoRaise(false);
    ui->setBgColor->setAutoRaise(false); TODO*/
    #endif

    // Show the HOME page
    ui->screenswitch->setCurrentIndex(3);

    ReadOptions();

    // SpectrumOnly mode support
//    if (ui->ribbon->isHidden())
//        ui->docView->setUsePageMode(false);
//    else
        //ui->docView->setUsePageMode(true);

    //ui->quickWidget->rootObject()->setProperty("appColor", "#2b579a");
    //ui->quickWidget->rootObject()->setProperty("appIcon", "../AppIcons/blackandwhite/WinWordLogoSmall.scale-180.png");
    //ui->quickWidget->rootObject()->setProperty("appName", "O20.Word");

    // showApp splash screen with fade effect
    /*if (ui->displaysplashscreen->isChecked()) {
         ShowScreen("SPLASH");

	 showApp();
         //ui->spectrumSearch->setPlaceholderText("Getting things ready...");
	 QTest::qWait(1000);
    } else {
         ShowScreen("START");
    }*/

    ui->readingBrowser->setUsePageMode(true);
    ui->readingBrowser->setTextInteractionFlags(ui->readingBrowser->textInteractionFlags() | Qt::TextSelectableByKeyboard);
    // TODO ui->readingBrowser->setBackgroundColor("#262626");

    ui->ribbon->setCurrentIndex(1);
    ui->Home->setChecked(true);
    ui->startHome->setChecked(true);
    CreateRibbonMenus();
    ui->viewModes->setCurrentIndex(0);
    ui->switcher->setCurrentIndex(0);

    ui->readingBrowser->setUsePageMode(true);
    // ??? TODO ui->readingBrowser->setPageContinuous(true);
    ui->docView->installEventFilter(this);

    GenerateAutoStyles();
    startCoreConnections();
    ui->fontSizeComboBox->setEditText(QString::number(10));
    SetupSpectrum();
    CreateSpellCheckerObject();  
}

void O20Gui::UpdateAutosave() {
    autosave = ui->switch_2->rootObject()->findChild<QObject*>("Switch")->property("checked").toBool();
    ui->saveButton->setHidden(autosave);
    slotSetTitle();
}

void O20Gui::CreateSpellCheckerObject() {
	ui->doSpellCheck->setChecked(true);
	docSpellChecker = new Sonnet::SpellCheckDecorator(ui->docView);
	spellCheckOn = true;
}

void O20Gui::DeleteSpellCheckerObject() {
	if (spellCheckOn) {
            delete docSpellChecker->highlighter();
            delete docSpellChecker; 
        }

	spellCheckOn = false;
	ui->doSpellCheck->setChecked(false);
}

void O20Gui::startCore()
{
   if (!ui->showStartScreen->isChecked() or document.filename != "") nostart = true;
   if (nostart)
       ShowScreen("EDITNOANIM");
   else
       ShowScreen("STARTNOANIM");
}

/* Destructor: delete UI */
O20Gui::~O20Gui()
{
    delete ui;
}

/* Why is this here? */
void O20Gui::showApp()
{
    show();
}

void O20Gui::CreateRibbonMenus()
{
    QAction* autohideribbon = new QAction(tr("Hide Ribbon"), this);
    QAction* showtabs = new QAction(tr("Show Tabs Only"), this);
    QAction* showtabsandcommands = new QAction(tr("Show Tabs and Commands"), this);
    QAction* showsidebar = new QAction(tr("Show Sidebar"), this);
    QMenu* ribboncontrolmenu = new QMenu(this);
    ribboncontrolmenu->addAction(autohideribbon);
    ribboncontrolmenu->addAction(showtabs);
    ribboncontrolmenu->addAction(showtabsandcommands);
    ribboncontrolmenu->addAction(showsidebar);
    ui->ribboncontrol->setMenu(ribboncontrolmenu);
    connect(autohideribbon, SIGNAL(triggered()), SLOT(HideRibbon()));
    connect(showtabs, SIGNAL(triggered()), SLOT(HideTabs()));
    connect(showtabsandcommands, SIGNAL(triggered()), SLOT(ShowTabs()));
    connect(showsidebar, SIGNAL(triggered()), SLOT(ShowSidebar()));

    QMenu* listMenu = new QMenu(this);

    bulletList.append(listMenu->addAction(tr("Black Circle")));
    bulletList.append(listMenu->addAction(tr("White Circle")));
    bulletList.append(listMenu->addAction(tr("Square")));
    bulletList.append(listMenu->addAction("1. 2. 3."));
    bulletList.append(listMenu->addAction("a. b. c."));
    bulletList.append(listMenu->addAction("A. B. C."));
    bulletList.append(listMenu->addAction("i. ii. iii."));
    bulletList.append(listMenu->addAction("I. II. III."));

    for (QAction* action : bulletList) {
        connect(action, SIGNAL(triggered()), SLOT(setTextListOn()));
    }

    connect(ui->bulletedlist, SIGNAL(released()), SLOT(setTextListOn()));
    ui->bulletedlist->setMenu(listMenu);

    QMenu* underlineMenu = new QMenu(this);

    underlineList.append(underlineMenu->addAction(tr("No underline")));
    underlineList.append(underlineMenu->addAction("_________"));
    underlineList.append(underlineMenu->addAction("___ ___ __"));
    underlineList.append(underlineMenu->addAction("_ _ _ _ _ _"));
    underlineList.append(underlineMenu->addAction("___ _ ___ _"));
    underlineList.append(underlineMenu->addAction("___ _ _ ___"));
    underlineList.append(underlineMenu->addAction("~~~~~~~"));

    for (QAction* action : underlineList) {
        connect(action, SIGNAL(triggered()), SLOT(setTextUnderlineStyle()));
    }

    connect(ui->setUnderline, SIGNAL(released()), SLOT(setTextUnderlineStyle()));
    ui->setUnderline->setMenu(underlineMenu);

    QMenu* indentMenu = new QMenu(this);
    QAction* noSI = new QAction("No indent");
    QAction* FI = new QAction("First line indent");
    QAction* HI = new QAction("Hanging indent");

    indentMenu->addAction(noSI);
    indentMenu->addAction(FI);
    indentMenu->addAction(HI);

    ui->specialIndent->setMenu(indentMenu);


    ibefore = -1;
    connect(noSI, &QAction::triggered, [=]() {
        QTextCursor cursor = ui->docView->textCursor();
        QTextBlockFormat fmt = cursor.blockFormat();
        fmt.setTextIndent(0);
	if (ibefore != -1) fmt.setLeftMargin(ibefore), ibefore = -1;
        cursor.setBlockFormat(fmt);
        ui->docView->setTextCursor(cursor);
    });

    connect(FI, &QAction::triggered, [=]() {
        QTextCursor cursor = ui->docView->textCursor();
        QTextBlockFormat fmt = cursor.blockFormat();
        fmt.setTextIndent(50);
	if (ibefore != -1) fmt.setLeftMargin(ibefore), ibefore = -1;
        cursor.setBlockFormat(fmt);
        ui->docView->setTextCursor(cursor);
    });

    connect(HI, &QAction::triggered, [=]() {
        QTextCursor cursor = ui->docView->textCursor();
        QTextBlockFormat fmt = cursor.blockFormat();
	ibefore = fmt.leftMargin();
	fmt.setLeftMargin(50);
	fmt.setTextIndent(-25);
        cursor.setBlockFormat(fmt);
        ui->docView->setTextCursor(cursor);
    });


    QMenu* insertTable = new QMenu(this);

    QWidgetAction* hact = new QWidgetAction(this);
    QWidgetAction* wact = new QWidgetAction(this);

    tableh->setValue(3);
    tablew->setValue(5);

    hact->setDefaultWidget(tableh);
    wact->setDefaultWidget(tablew);

    tableh->setStyleSheet("#tableh {padding: 5px;}");

    insertTable->addAction(hact);
    insertTable->addAction(wact);

    ui->insertTable->setMenu(insertTable);

    QMenu* insertLinkMenu = new QMenu(this);
    QWidgetAction* urlact = new QWidgetAction(this);
    QWidgetAction* urlnameact = new QWidgetAction(this);

    url->setPlaceholderText(tr("Link Address"));
    urlname->setPlaceholderText(tr("Display Text"));

    urlact->setDefaultWidget(url);
    urlnameact->setDefaultWidget(urlname);

    url->setStyleSheet("QLineEdit { border: none; background: transparent; padding: 5px; }");
    urlname->setStyleSheet("QLineEdit { border: none; background: transparent; padding: 5px; }");

    insertLinkMenu->addAction(urlact);
    insertLinkMenu->addAction(urlnameact);
    ui->insertLink->setMenu(insertLinkMenu);

    QMenu* lineSpacing = new QMenu(this);
   
    lineSpacing->setTitle("OOOOOOOOOOOO");
    spacingList.append(lineSpacing->addAction("1.0"));
    spacingList.append(lineSpacing->addAction("1.15"));
    spacingList.append(lineSpacing->addAction("1.5"));
    spacingList.append(lineSpacing->addAction("2.0"));
    spacingList.append(lineSpacing->addAction("2.5"));

    for (QAction* action : spacingList) {
        connect(action, SIGNAL(triggered()), SLOT(setTextSpacing()));
    }

    ui->spacingButton->setMenu(lineSpacing);

   // QMenu* indentMenu = new QMenu(this);
   // indentMenu

    QMenu* clipboard = new QMenu(this);
    connect(clipboard->addAction(QIcon(":/axialis/Basicclipboard-cut_small@1x.png"), tr("Cut")), &QAction::triggered, ui->docView, &QTextEdit::cut);
    connect(clipboard->addAction(QIcon(":/axialis/Basic/clipboard-copy_small@1x.png"), tr("Copy")), &QAction::triggered, ui->docView, &QTextEdit::copy);
    connect(clipboard->addAction(QIcon(":/axialis/Basic/small/clipboard-paste.png"), tr("Paste")), &QAction::triggered, ui->docView, &QTextEdit::paste);
    connect(clipboard->addAction(QIcon(":/axialis/Basic/clipboard-paste-text-only_small@1x.png"), tr("Paste (Text Only)")), &QAction::triggered, 
		    [=]() {
		       const QClipboard *clipboard = QApplication::clipboard();
                       const QMimeData *mimeData = clipboard->mimeData();
		       this->ui->docView->insertPlainText(mimeData->text());
		    });
    ui->clipboard->setMenu(clipboard); 

    QMenu *setColorMenu = new QMenu(this);
    connect(setColorMenu->addAction(QIcon(":/Style/colorpics/default.png"), tr("Automatic")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.clearForeground();ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Red.png"), tr("Red")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#E51400"));ui->docView->setCurrentCharFormat(a);});

    connect(setColorMenu->addAction(QIcon(":/Style/colors/Crimson.png"), tr("Crimson")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#A20025"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Magenta.png"), tr("Magenta")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#D80073"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Mauve.png"), tr("Mauve")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#76608A"));ui->docView->setCurrentCharFormat(a);});

    connect(setColorMenu->addAction(QIcon(":/Style/colors/Orange.png"), tr("Orange")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#FA6800"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Yellow.png"), tr("Yellow")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#E3C800"));ui->docView->setCurrentCharFormat(a);});

    connect(setColorMenu->addAction(QIcon(":/Style/colors/Pink.png"), tr("Pink")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#F472D0"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Violet.png"), tr("Violet")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#AA00FF"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Cobalt.png"), tr("Cobalt")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#3E65FF"));ui->docView->setCurrentCharFormat(a);});

    connect(setColorMenu->addAction(QIcon(":/Style/colors/Indigo.png"), tr("Indigo")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#6A00FF"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Teal.png"), tr("Teal")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#00ABA9"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Steel.png"), tr("Steel")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#647687"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Emerald.png"), tr("Emerald")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#008A00"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Olive.png"), tr("Olive")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#6D8764"));ui->docView->setCurrentCharFormat(a);});

    connect(setColorMenu->addAction(QIcon(":/Style/colors/Green.png"), tr("Green")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#60A917"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Cyan.png"), tr("Cyan")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#1BA1E2"));ui->docView->setCurrentCharFormat(a);});
    connect(setColorMenu->addAction(QIcon(":/Style/colors/Taupe.png"), tr("Taupe")), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setForeground(QBrush("#87794E"));ui->docView->setCurrentCharFormat(a);});

    connect(setColorMenu->addAction(QIcon(":/axialis/Images/small/tool-color-sampler-filled.png"), tr("Select Color...")), &QAction::triggered, ui->docView, [=]() {
		    QTextCharFormat a = ui->docView->currentCharFormat();
		    QColor color = QColorDialog::getColor(a.foreground().color(), this, tr("Select Color"));
                    a.setForeground(QBrush(color));
                    ui->docView->setCurrentCharFormat(a);
                    });
    ui->setColor->setMenu(setColorMenu);

    QMenu *setBColorMenu = new QMenu(this);

    /*
    connect(setBColorMenu->addAction(QIcon(":/Style/colors/Yellow.png"), "Yellow"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setBackground(QBrush("#E3C800"));ui->docView->setCurrentCharFormat(a);});
    connect(setBColorMenu->addAction(QIcon(":/Style/colors/Green.png"), "Green"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setBackground(QBrush("#60A917"));ui->docView->setCurrentCharFormat(a);});
    connect(setBColorMenu->addAction(QIcon(":/Style/colors/Cyan.png"), "Cyan"), &QAction::triggered, [=]() {
        QTextCharFormat a = ui->docView->currentCharFormat();a.setBackground(QBrush("#1BA1E2"));ui->docView->setCurrentCharFormat(a);}); */

    connect(setBColorMenu->addAction(QIcon(":/axialis/Images/small/tool-color-sampler-filled.png"), tr("Select Color...")), &QAction::triggered, ui->docView, [=]() {
                    QTextCharFormat a = ui->docView->currentCharFormat();
                    QColor color = QColorDialog::getColor(a.background().color(), this, tr("Select Color"));
                    a.setBackground(QBrush(color));
                    ui->docView->setCurrentCharFormat(a);
                    });
    connect(setBColorMenu->addAction(QIcon(":/axialis/Images/small/transparency.png"), tr("Transparent")), &QAction::triggered, ui->docView, [=]() {
                    QTextCharFormat a = ui->docView->currentCharFormat();
                    a.clearBackground();
                    ui->docView->setCurrentCharFormat(a);
                    });

    ui->setBgColor->setMenu(setBColorMenu);

}


void O20Gui::ShowSidebar()
{
    if (!ui->sidebarframe->isHidden()) return;
    if (ui->slimSidebar->isHidden()) ui->slimSidebar->show();

    updateNavigation();
    ui->editsidebar->setCurrentIndex(0);
    ui->helpindex->setCurrentIndex(0);

    ui->sidebarframe->show();
    /*QPropertyAnimation *animation = new QPropertyAnimation(ui->sidebarframe, "pos");
    animation->setDuration(100);
    animation->setStartValue(ui->sidebarframe->pos().x());
    animation->setEndValue(QPoint(ui->sidebarframe->pos().x()+ui->sidebarframe->width(), ui->sidebarframe->pos().y()));*/

    // to slide in call
    //animation->start();
    //connect(animation, &QPropertyAnimation::finished, [=](){ this->ui->sidebarframe->show(); });
}

void O20Gui::HideRibbon()
{
    ui->ribbon->hide();
    ui->statusbar->hide();
    tabshidden = false;
    ui->docView->setUsePageMode(false);
    /*ui->user->hide(), ui->ribboncontrol->hide();*/
}

void O20Gui::ShowTabs()
{
    //if (plaintextmode) return;
    ui->ribbon->setMaximumSize(16777215, 16777215);
    ui->ribbon->show();
    ui->statusbar->show();
    tabshidden = false;
    if (ui->pageview->isChecked()) ui->docView->setUsePageMode(true);
    else ui->docView->setUsePageMode(false);
    /*ui->user->show(), ui->ribboncontrol->show();*/
}

void O20Gui::HideTabs()
{
    ui->ribbon->setMaximumSize(16777215, ribbonminsize);
    ui->ribbon->show();
    ui->statusbar->show();
    tabshidden = true;
    if (ui->pageview->isChecked()) ui->docView->setUsePageMode(true);
    else ui->docView->setUsePageMode(false);
    /*ui->user->show(), ui->ribboncontrol->show();*/
}

void O20Gui::Dictation()
{
    /*if (sender() == ui->dictation) {
        if (ui->dictation->isChecked()) speech->say(ui->docView->toPlainText());
	else speech->stop();
    } else if (sender() == speech && speech->state() == QTextToSpeech::Ready) {
        ui->dictation->setChecked(false);
    }*/
}

void O20Gui::SetView()
{
    if (sender() != ui->readingMode) ui->pageStack->setCurrentIndex(0);

    if (sender() == ui->pageview) ui->docView->setUsePageMode(true), ui->docView->setPageContinuous(false);
    else if (sender() == ui->webview) ui->docView->setUsePageMode(false);
    else if (sender() == ui->scrollview) ui->docView->setUsePageMode(true), ui->docView->setPageContinuous(true);
    else if (sender() == ui->readingMode) {
        ui->readingBrowser->document()->setHtml(ui->docView->document()->toHtml());
        ui->pageStack->setCurrentIndex(1);

	ui->quickDock->show();
        ribbonShownBefore = ui->ribbon->isVisible();
	statusBarShownBefore = ui->statusbar->isVisible();
        topbarShownBefore = ui->windec->isVisible();

	ui->ribbon->hide();
	ui->statusbar->hide();
	ui->windec->hide();
	isReadingMode = true;
        //if (ui->showInFullscreen->isChecked()) showFullScreen();
        //else showNormal();
	//showFullScreen();
    }

    //} else if (sender() == ui->audioMode) {
    //    ui->viewModes->setCurrentIndex(1);
    //    reader.say(ui->docView->toPlainText());
    //    //ui->texttoread->setPlainText(ui->docView->toPlainText());        
    //}
}

#include <QFuture>
#include <QtConcurrent>

void O20Gui::slotSetTitle() {
    setWindowTitle("");
    if (autosave && document.filename != "" && !readOnly) {
        //SaveDocument();
	//QObject* myObject = new QObject;
	//QFuture<void> future = QtConcurrent::run([=]() { SaveDocument(); });

        if (!locksave) locksave = true;
        else return;

        QObject* myObject = new QObject;
        QThread* threadt = new QThread;
        threadt->setObjectName("DocumentSaveThread");
        QObject::connect(threadt, &QThread::started, [=]() { SaveDocument(); threadt->terminate();});

        myObject->moveToThread(threadt);
        threadt->start();
    }

    int wordCount = ui->docView->toPlainText().split(QRegExp("(\\s|\\n|\\r)+"), QString::SkipEmptyParts).count();
    int pageCount = ui->docView->document()->pageCount();
    ui->charcount->setText(QString(tr("%1 pages    %2 words  ")).arg(pageCount).arg(wordCount));
		    //QString::number(pageCount) + (pageCount == 1 ? tr(" page    ") : tr(" pages    ")) + QString::number(wordCount) + tr(" words  "));
}

void O20Gui::SidebarSwitcher()
{
    //ui->startDecorations->setStyleSheet("");
    if (sender() == ui->Home || sender() == ui->startHome) {
        ui->switcher->setCurrentIndex(0);
	//ui->startDecorations->setStyleSheet("background-color: white;");
	/*if (QTime::currentTime().hour() < 12)
            ui->greeting->setText("Good Morning");
        else if (QTime::currentTime().hour() < 18)
            ui->greeting->setText("Good Afternoon");
	else
            ui->greeting->setText("Good Evening");*/

    } else if (sender() == ui->New || sender() == ui->startNew)   ui->switcher->setCurrentIndex(1);
    else if (sender() == ui->Open || sender() == ui->startOpen) ui->switcher->setCurrentIndex(2);
    else if (sender() == ui->Options || sender() == ui->startOptions) ui->switcher->setCurrentIndex(5);
    else if (sender() == ui->Save) ui->switcher->setCurrentIndex(3);
    else if (sender() == ui->About || sender() == ui->startAbout) ui->switcher->setCurrentIndex(4);
}

void O20Gui::OpenFileLocation()
{
    if (document.filename != "")
        QDesktopServices::openUrl(QUrl::fromLocalFile(QFileInfo(document.filename).path()));
}

void O20Gui::ShowScreen(QString name, bool forceshow)
{
    if (name == "SPLASH") ui->screenswitch->setCurrentIndex(2);
    if (name == "FILE") ui->screenswitch->slideInIdx(defaultHome);
    if (name == "EDIT") ui->screenswitch->slideInIdx(1);
    if (name == "EDITNOANIM") ui->screenswitch->setCurrentIndex(1);
    if (name == "EDITFADE") ui->screenswitch->fadeInIdx(1);
    if (name == "EDIT" && ui->screenswitch->currentIndex() == 0) ui->ribbon->setCurrentIndex(1);

    if (name == "START") {
        ui->screenswitch->slideInIdx(defaultHome);
        /* Maybe do something else... */
    } else if (name == "STARTNOANIM") {
        ui->screenswitch->setCurrentIndex(defaultHome);
    } else if (name == "STARTFADE") {
        ui->screenswitch->fadeInIdx(defaultHome);
    }

    if (name == "SPECTRUM") {
        ui->screenswitch->slideInIdx(2);
        /* Maybe do something else... */
    } else if (name == "SPECTRUMNOANIM") {
        ui->screenswitch->setCurrentIndex(2);
    } else if (name == "SPECTRUMFADE") {
        ui->screenswitch->fadeInIdx(2);
    }

    if (nostart) {
        ui->startSidebar->close();
        ui->sidebars->setCurrentIndex(1);
	ui->sidebar->show();
        ui->backtoedit->show();
	nostartDone = true;
    } else {
        ui->sidebars->setCurrentIndex(0);
    }
}

void O20Gui::SwitchScreens()
{
    if ((sender() == ui->ribbon && ui->ribbon->currentIndex() == 0) or sender() == ui->gobackremode) {
        ShowScreen("START");
	ui->switcher->setCurrentIndex(0);
	ui->Home->setChecked(true);
	//ui->startDecorations->setStyleSheet("#startDecorations { background-color: white; }");
    } else if (sender() == ui->goback or sender() == ui->backtoedit) {
        ShowScreen("EDIT");
	ui->ribbon->setCurrentIndex(1);
    }
}

void O20Gui::CloseSidebar()
{
    //ui->sidebarframe->setGeometry(ui->sidebarframe->width(), 0, ui->sidebarframe->width(), ui->sidebarframe->height());
    if (ui->sidebarframe->isHidden()) {
	    if (sender() == ui->closeEntireSidebar) ui->slimSidebar->hide();
	    return;
    }

    bool a = false;
    if (sender() == ui->closeEntireSidebar) a = true;

    // then a animation:
    /*QPropertyAnimation *animation = new QPropertyAnimation(ui->sidebarframe, "geometry");
    animation->setDuration(100);
    animation->setStartValue(ui->sidebarframe->geometry());
    animation->setEndValue(QRect(ui->sidebarframe->x(), ui->sidebarframe->y(), 0, ui->sidebarframe->height()));

    // to slide in call
    animation->start();
    connect(animation, &QPropertyAnimation::finished, [=]() {*/
	    ui->sidebarframe->hide();
	    if (a) ui->slimSidebar->hide();
    //});
}

void O20Gui::HelpIndex()
{
    if (sender() == ui->helphome) {
        ui->helpindex->slideInIdx(0);

        if (ui->sidebarframe->isHidden()) ShowSidebar();
	else {
	    if (ui->editsidebar->currentIndex() == 0)
                CloseSidebar(), qDebug("oooo");
	}

	ui->editsidebar->setCurrentIndex(0);
	ui->helpindex->slideInIdx(0);
    } else if (sender() == ui->navButton) {
        if (ui->sidebarframe->isHidden()) ShowSidebar();
        else {
            if (ui->editsidebar->currentIndex() == 1)
                CloseSidebar();
	}

	ui->editsidebar->setCurrentIndex(1);
    }

    ShowSidebar();
    if (sender() == ui->whatsnew) {
        ui->helpindex->slideInIdx(1);
    } else if (sender() == ui->spectrumintro) {
        ui->helpindex->slideInIdx(2);
    } else if (sender() == ui->office20license) {
        ui->helpindex->slideInIdx(3);
    } else if (sender() == ui->aboutword) {
        ui->helpindex->slideInIdx(4);
    }
}
