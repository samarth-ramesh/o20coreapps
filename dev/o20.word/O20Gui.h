#ifndef WORDINTERFACE_H
#define WORDINTERFACE_H

#include <QMainWindow>
#include <QSpinBox>

#include <QTextCharFormat>
#include <QTextBlockFormat>

#include <QToolButton>
#include <QSystemTrayIcon>
#include <QList>

#include <theme.h>
#include <repository.h>
#include <syntaxhighlighter.h>

#include <Sonnet/SpellCheckDecorator>

#include <converter.h>
#include <Speak/SpectrumSpeak.h>

#include <QDialog>
#include <QDir>

QT_BEGIN_NAMESPACE
namespace Ui { class O20Gui; }
QT_END_NAMESPACE

class O20Gui : public QMainWindow
{
    Q_OBJECT

public:
    O20Gui(QWidget *parent = nullptr);
    ~O20Gui();

    bool openDocument(QString name, bool readonly = false, bool noEdit = false);
    void showApp();
    void startCore();

public slots:
    bool ioReceiver(QString mode = "open", QString where = "", bool lock = false);
    bool SaveDocument();

private:
   void createWidgets();

public slots:

   void CreateBlankFromTemplate(QString loc = "", QString whereto = "");

private:
    void LoadFileManager(QString path);

    QString generateNewDocPath();
    void CreateSpellCheckerObject();
    void DeleteSpellCheckerObject();

    void UpdateRecentFiles();
    void LoadRecentFiles();
    void AddRecentFile(bool force = false);
    void UpdateRecentFilesWidget();

    void RemoveList();
    void CustomizeQuickAccessToolbar();
    void CreateRibbonMenus();

    void UpdateStyles(QString stylename, bool autodetect = false);
    void SaveStyles(bool HTML_NOT_ODF = false);

    void startCoreConnections();
    void ReadOptions();
    void WriteOptions();

    void ShowScreen(QString name, bool forceshow = false);
    void SetupSpectrum();

    void ShowSwitch();
    void HideSwitch();
private slots:
    void enableEditing();
    void ShowSidebar();
    void HelpIndex();

    void updateNavigation();
    void UpdateAutosave();
    void getSpectrumAnswers();
    void addSpectrumChatEntry(QString icon, QString text);

    void ProvideContextMenu(const QPoint& pos);
    void TalkToSpectrum();
    void ConnectStyleSlots(QString stylename);

    void GenerateAutoStyles(OOO::StyleInformation* mStyleInformation = nullptr);

    void SetView();
    void SwitchScreens();
    void EditingCommands();
    // TODO void OpenFileLocation();

    void OpenFileLocation();

    void Dictation();

    void setTextListOn();
    void setTextSpacing();
    void setTextUnderlineStyle();

    void CloseSidebar();
    void SidebarSwitcher();
    void ShowTabs();
    void HideTabs();
    void HideRibbon();
    void slotSetTitle();

    void onCursorUpdate();
    void onTextChanged();

    bool checkReturnKey(QKeyEvent *keyEvent);
protected:
    //void SetWindowTitle();
    void setWindowTitle(QString name = "");
    void closeEvent(QCloseEvent* event);
    bool eventFilter(QObject *object, QEvent *event);

private:
    Ui::O20Gui *ui;

    QStringList searchlist;

    bool darkModeCode = false;
    bool hasAlreadyRecented = false;
    bool tabshidden;
    qreal ribbonminsize;

    bool nostart;
    QString defaultname = "Document1";
    struct DocumentData {
        QString filename;
	QString author;
	QString title;
	QString suffix;
    } document;

    bool spectrumOnly;

    QFileInfoList list;
    QVector<QAction*> bulletList;
    QVector<QAction*> spacingList;
    QVector<QAction*> underlineList;

    bool locksave = false;
    bool restrict = false;
    bool plaintextmode = false;

    bool ribbonShownBefore, statusBarShownBefore, topbarShownBefore;

    bool isReadingMode = false;
    bool readOnly = false;

    bool firstTimeCreated = false;
    int defaultHome = 0;

    QSpinBox *tableh, *tablew;

    QString greeter;

    QSystemTrayIcon *wordtray;

    bool wasMaximized;
    bool styleon = false;
    QStringList recentfilenames;
    QStringList stylenames;
    QMap<QString, QTextCharFormat> styleCharFormats;
    QMap<QString, QTextBlockFormat> styleBlockFormats;
    QLineEdit *url;
    QLineEdit *urlname;

    bool wasDeleted = false;
    QString usericon;
    bool alreadyPPS = false;
    bool wasPageBefore;
    bool isSidebarMinimized = true;

    Sonnet::SpellCheckDecorator* docSpellChecker;

    bool nostartDone = false;
    QList<QToolButton*> styleButtonList;
    QList<int> headingPosList;
    QList<int> commentPosList;
    QMenu* stylemenus;
    SpectrumSpeak reader;

    QDialog* charSelectDialog;
    QString uname = "";

    int ibefore = -1;
    bool spellCheckOn = true;
    bool noPhoto = true;
    bool saveOnExit = true;
    bool askSaveLocation = false;
    // TODO QString saveWhere = "";
    bool autosave = true;
    bool alwaysAutosave = true;
    KSyntaxHighlighting::Repository m_repository;
    KSyntaxHighlighting::SyntaxHighlighter *m_highlighter;
};

#endif // WORDINTERFACE_H
