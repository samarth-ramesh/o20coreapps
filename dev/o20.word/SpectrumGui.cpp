/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "O20Gui.h"
#include "src/ui_O20Ui.h"

#include <QtWidgets>
#include <QQuickItem>

#define O20AICON ":/axialis/Website and Email/small/assistance-1.png" 
#define USERICON ":/axialis/User Management/small/user-profile-man.png"

void O20Gui::TalkToSpectrum() {
     QString what = ui->spectrumOnlyInput->text();
     ui->spectrumOnlyInput->setText("");

     QString output = "Ask me anything";

     if (what == "") return;

     if (what.contains("save", Qt::CaseInsensitive) && nostart) {
               if (!nostart) output = "Try creating or opening a document to do that.";
               else if (document.filename != "") output = "I'm AutoSaving this document for you.";
               else if (what.contains("as", Qt::CaseInsensitive)) {
                   if (what.contains("pdf", Qt::CaseInsensitive))
                       ui->saveAsPDF->click();
                   else
                      ui->saveAs->click();
               } else
                   ui->save->click();
     } else if (what.contains("open", Qt::CaseInsensitive)) {
                if (what.contains("location", Qt::CaseInsensitive))
                   if (document.filename == "")
                       output = "Try saving this document somewhere so I can show your where it is!";
                   else
                       ui->openfilelocation->click();
               else
                   ui->openButton->click();
     } else if (what.contains("print", Qt::CaseInsensitive)) ui->print->click();
    else if (what.contains("create", Qt::CaseInsensitive) or what.contains("make", Qt::CaseInsensitive)) {
            ui->blankfromtemplate->click();
    } else output = "Sorry, I can't do that!";
    ui->spectrumOnlyInput->setPlaceholderText(output);
}

void O20Gui::SetupSpectrum()
{
    QString startsay;
    bool starter = true;

    uname = ui->userNickname->text();

    if (searchlist.size() == 0) {
       startsay = "I'm Spectrum.";
    } else {
       starter = false;
       startsay = tr("Welcome back!");
       if (uname != "") startsay = tr("Welcome back, %1!").arg(uname.section(' ', 0, 0));
    }

    searchlist = QStringList() << tr("italic") << tr("bold") << tr("strikeout") << tr("underline") <<
	    tr("go to #") << tr("go to line #") << tr("go to top") << tr("go to bottom") <<
	    tr("align left") << tr("align right") << tr("align center") << tr("align justify") << tr("left") <<
	    tr("right") << tr("center") << tr("justify") << tr("rename PATH");

    QCompleter *askme = new QCompleter(searchlist);
    askme->setCaseSensitivity(Qt::CaseInsensitive);


    ui->spectrumChat->clear();
    QStandardItemModel* model = new QStandardItemModel();

    // initialize the model
    int rows = searchlist.count();  // assuming this is a QStringList
    model->setRowCount(rows);
    model->setColumnCount(1);

    // load the items
    int row = 0;
    QString name;
    foreach(name, searchlist) {
        QStandardItem* item = new QStandardItem(name);
        item->setIcon(QIcon(":/basic/clock_small@1x.png"));
        model->setItem(row, 0, item);
       row++;
    }

    //askme->setModel(model);
    //askme->popup()->setModel(model); // may or may not be needed
    askme->setCompletionMode(QCompleter::InlineCompletion);
    ui->spectrumSearchInput->setCompleter(askme);

    /* Add DT */
    QStringList greetings;
    greetings << tr("This is O20.") << tr("Hi.") << tr("Hello there.") <<
	    tr("Hello!") << tr("Greetings!") << tr("Hey there!") << tr("Welcome.") << tr("Good day.") << tr("Let's get started.");

    int greet = QRandomGenerator::global()->bounded(greetings.size());
    greeter = greetings.at(greet);
    if (greeter == tr("Welcome.") && !starter) {
        greeter = tr("Welcome back!");
        if (uname != "") {
            greeter = QString(tr("Welcome back, %1!")).arg(uname.section(' ', 0, 0)); //"Welcome back, " + uname.section(' ', 0, 0) + "!";
        }
    }

    //if (ui->optionsUserName->text() != "") ui->name->hide();

    QStringList afterask;
    afterask << "Let's try another one!" << "Have another question?" << "Need some more help?" << "I'm here to help." << "What can I do now?" << "Ask me anything." <<
                "Need something?" << "What's on your mind?" << "Anything I can do for you?" << "How can I help?";

    /*QDate date = QDate::currentDate();

    if (greeter == "DT") {
        if (date.dayOfWeek() == 1) greeter = "Happy Monday!";
        else if (date.dayOfWeek() == 2) greeter = "Happy Tuesday!";
        else if (date.dayOfWeek() == 3) greeter = "Happy Wednesday!";
        else if (date.dayOfWeek() == 4) greeter = "Happy Thursday!";
        else if (date.dayOfWeek() == 5) greeter = "Happy Friday!";
        else if (date.dayOfWeek() == 6) greeter = "Happy Saturday!";
        else if (date.dayOfWeek() == 7) greeter = "Happy Sunday!";
    }*/

    /*if (date.month() == 1 && date.day() == 1) greeter = "Happy New Year!";
    else if (date.month() == 12 && date.day() == 25) greeter = "Happy Cristmas!";*/

    ui->greeting->setText(greeter);
    //ui->spectrumname->setText(greeter);
    ui->spectrumOnlyGreeting->setText(greeter);

    ui->spectrumSearch->setText(startsay);
    //ui->spectrumReply->insertPlainText(startsay);

    QStringList filesearchlist;
    filesearchlist << "save" << "save as" << "save as pdf" << "save as web" << "save as html" << "open document" << "open exisiting" <<
                      "open location" << "print" << "delete document" << "delete file" << "create new document";// << "make new document";
    QCompleter *filesearch = new QCompleter(filesearchlist);
    filesearch->setCaseSensitivity(Qt::CaseInsensitive);
    filesearch->setCompletionMode(QCompleter::InlineCompletion);
    //ui->talkToSpectrum->setCompleter(filesearch);

    //addSpectrumChatEntry(O20AICON, "Welcome to O20");
    ui->spectrumChat->hide();
    ui->welcome->show();
    ui->spectrumInput->setCompleter(askme);
    connect(ui->spectrumOnlyInput, SIGNAL(returnPressed()), SLOT(TalkToSpectrum()));
    connect(ui->spectrumSearchInput, &QLineEdit::returnPressed, this, &O20Gui::getSpectrumAnswers);
    connect(ui->spectrumInput, &QLineEdit::returnPressed, this, &O20Gui::getSpectrumAnswers);
    connect(ui->spectrumChat, &QListWidget::itemClicked, [=]() {
		    ui->spectrumSearchInput->setText(ui->spectrumChat->currentItem()->text());
		    });

/*    connect(ui->spectrumSearchInput, &QLineEdit::returnPressed, [=]() {
		        QString what = ui->spectrumSearchInput->text();
			//ui->spectrumReply->insertPlainText("\n");
			//ui->spectrumReply->insertHtml("<p style='color: #c6c6c6;'>" + what + "</p>");
		        ui->spectrumSearchInput->setText("");
                        //ui->spectrumSearch->clearFocus();

                        QListWidgetItem* input = new QListWidgetItem(what);
                        ui->spectrumChat->addItem(input);

                        bool error = false;
                        bool set = false, unset = false, show = false, make = false, hide = false, answer = false, greeting = false, is = false, close = false;

                        unset = (what.contains("unset", Qt::CaseInsensitive)) ? true : false;
                        set = (what.contains("set", Qt::CaseInsensitive) && !unset) ? true : false;
                        show = (what.contains("show", Qt::CaseInsensitive)) ? true : false;
                        hide = (what.contains("hide", Qt::CaseInsensitive) or what.contains("close", Qt::CaseInsensitive) or
                               (show && (what.contains("dont", Qt::CaseInsensitive) or
                                         what.contains("don't", Qt::CaseInsensitive)))) ? true : false;
                        show = (hide) ? false : show;
                        make = (what.contains("make", Qt::CaseInsensitive)) ? true : false;
                        answer = (what.contains("what", Qt::CaseInsensitive) or
                                what.contains("tell", Qt::CaseInsensitive)) ? true : false;
                        //is = (what.contains("is", Qt::CaseInsensitive)) ? true : false;

                        int oo = QRandomGenerator::global()->bounded(afterask.size());
                        QString emoji  = ":/Emoticon/face-smile.svg";
			QString output = "Done!"; //afterask.at(oo);
			QString furtherinfo = "<p style='font-size: 10pt;'>If that didn't work, try rephrasing, or just say 'Help!'</p>";
                        if (plaintextmode) furtherinfo += "<p style='font-size: 10pt; color: #e84025;'>You seem to be editing a plain text file. In plain text mode, some of my features are disabled.</p>";


                        if (what.contains("undo", Qt::CaseInsensitive)) {
                             ui->docView->undo();
                        } else if (what.contains("redo", Qt::CaseInsensitive)) {
                            ui->docView->redo();
                        //} else if (what.contains("hey", Qt::CaseInsensitive) or 
                        //         what.contains("hello", Qt::CaseInsensitive) or 
                        //         (what.contains("hi", Qt::CaseInsensitive) and !hide)) {
                        //    ShowScreen("SPECTRUM");
                        //    
                        //    ui->spectrumname->setText("Hi! I'm Spectrum.");
                        //    if (ui->optionsUserName->text() != "")
                        //        ui->spectrumname->setText("Hi, " + ui->optionsUserName->text().section(' ', 0, 0) + "! I'm Spectrum.");
                        //else if (what.contains("whats new", Qt::CaseInsensitive)) ui->docView->undo();
                        } else if (what.contains("exit", Qt::CaseInsensitive)) this->close();
                        else if (what.contains("center", Qt::CaseInsensitive)) {
                            ui->aligncenter->click();
                        } else if (what.contains("justify", Qt::CaseInsensitive)) {
                            ui->alignjustify->click();
                        } else if (what.contains("left", Qt::CaseInsensitive) and what.contains("align", Qt::CaseInsensitive)) {
                            ui->alignleft->click();
                        } else if (what.contains("right", Qt::CaseInsensitive) and what.contains("align", Qt::CaseInsensitive)) {
                            ui->alignright->click();
                        } else if (what.contains("save", Qt::CaseInsensitive)) {
                           if (what.contains("as", Qt::CaseInsensitive)) {
                                if (what.contains("pdf", Qt::CaseInsensitive))
                                    ui->saveAsPDF->click();
                                else
                                    ui->saveAs->click();
                            } else {
                                if (document.filename == "") 
                                    ui->save->click();
                                else
                                    output = "I'm AutoSaving your document, so you don't need to save it yourself.";
                            }
                        } else if (what.contains("open", Qt::CaseInsensitive)) {
                            if (what.contains("location", Qt::CaseInsensitive))
                                ui->openfilelocation->click();
                            else if (what.contains("document", Qt::CaseInsensitive) or what.contains("file", Qt::CaseInsensitive) or
                                     what.contains("existing", Qt::CaseInsensitive))
                                ui->openButton->click();
                            else
                                output = "What do you want to open?";
                        } else if (what.contains("print", Qt::CaseInsensitive)) ui->print->click();
                        else if (what.contains("create", Qt::CaseInsensitive)) ui->blankfromtemplate->click();
                        else if (what.contains("paste", Qt::CaseInsensitive)) ui->docView->paste();
                        else if (what.contains("copy", Qt::CaseInsensitive)) ui->docView->copy();
                        else if (what.contains("cut", Qt::CaseInsensitive)) ui->docView->cut();
                        else if (what.contains("italic", Qt::CaseInsensitive) or what.contains("accent", Qt::CaseInsensitive)) {
                            if (set or make) ui->docView->setTextItalic(true);
                            else if (unset) ui->docView->setTextItalic(false);
                            //else if (is) output = QString(ui->setItalic->isChecked() ? "Yes, this text is italic." : "No, it isn't italic.");
                            else ui->docView->setTextItalic(!ui->setItalic->isChecked());
                        } else if (what.contains("bold", Qt::CaseInsensitive) or what.contains("emphasize", Qt::CaseInsensitive)) {
                            if (set or make) ui->docView->setTextBold(true);
                            else if (unset) ui->docView->setTextBold(false);
                            //else if (is) output = QString(ui->setBold->isChecked() ? "Yes, this text is bold." : "No, it isn't bold.");
                            else ui->docView->setTextBold(!ui->setBold->isChecked());
                        } else if (what.contains("underline", Qt::CaseInsensitive)) {
                            if (set or make) ui->docView->setTextUnderline(true);
                            else if (unset) ui->docView->setTextUnderline(false);
                            //else if (is) output = QString(ui->setUnderline->isChecked() ? "Yes, this text is underlined." : "No, it isn't underlined.");
                            else ui->docView->setTextUnderline(!ui->setUnderline->isChecked());
                        } else if (what.contains("strikeout", Qt::CaseInsensitive) or
                                   what.contains("strikethrough", Qt::CaseInsensitive)) {
                            if (set or make) ui->docView->setTextStrikeOut(true);
                            else if (unset) ui->docView->setTextStrikeOut(false);
                            //else if (is) output = QString(ui->setStrikeout->isChecked() ? "Yes, this text is strikethrough." : "No, it isn't strikethrough.");
                            else ui->docView->setTextStrikeOut(!ui->setStrikeout->isChecked());
                        } else if (answer) {
                            if (what.contains("time", Qt::CaseInsensitive))
                                output = "The time is " + QTime::currentTime().toString("H:m:s a");
                            else if (what.contains("date", Qt::CaseInsensitive)) 
                                output = "Today is " + QDate::currentDate().toString("MMMM d yyyy");
                            else if (what.contains("day", Qt::CaseInsensitive))
                                output = "It's " + QDate::currentDate().toString("dddd!");
                            else if (what.contains("name", Qt::CaseInsensitive)) {
                                if (what.contains("your", Qt::CaseInsensitive)) output = "My name is Spectrum. You can ask me anything.";
                            }
                        } else if (show or hide) {
                            if (what.contains("ribbon", Qt::CaseInsensitive)) {
                                if (show) ShowTabs();
                                else HideRibbon();
                                if (plaintextmode) output = "Sorry, the ribbon is disabled when viewing plain text files. Try 'show tabs only' instead, or 'show file tab'.";
                            } else if (what.contains("tabs only", Qt::CaseInsensitive)) {
                               if (show) HideTabs();
                               else output = "Sorry, I can't do that.";
                            } else if (what.contains("sidebar", Qt::CaseInsensitive) or what.contains("help", Qt::CaseInsensitive)) {
                               if (show) ShowSidebar();
                               else CloseSidebar();
                            } else if (what.contains("file tab", Qt::CaseInsensitive) or what.contains("home", Qt::CaseInsensitive)) {
                                 ShowScreen("START");
                                 ui->switcher->setCurrentIndex(0);
                                 ui->Home->setChecked(true);
                            }
                        } else if (what.contains("help", Qt::CaseInsensitive)) {
                            ShowSidebar();
                            ui->helpindex->setCurrentIndex(2);
                            output = "Need some more help?";
                        } else if (what.contains("thanks", Qt::CaseInsensitive)) {
                            emoji = ":/Emoticon/face-embarrassed.svg";
			    output = "Don't mention it.";
			    furtherinfo = "After all, I'm here to help!";
                        } else {
                            output = "Sorry, I didn't get that.", error = true;
			    furtherinfo = "Try rephrasing, or say 'Help' for some examples.";
			    ui->spectrumSearch->setText("<body>"+output+"</body>");
			    //ui->furtherInfo->setText(furtherinfo);
			    QListWidgetItem* item = new QListWidgetItem(output);
			    ui->spectrumChat->addItem(item);
			    //ui->spectrumEmoji->setIcon(QIcon(":/Emoticon/face-confused.svg"));
                            return;
			}

			//ui->spectrumReply->insertPlainText("\n");
                        //ui->spectrumReply->insertHtml("<p style='color: #414445;'>" + output + "</p>");
                        //ui->spectrumSearch->setText("<body>"+output+"</body>");
         	        QListWidgetItem* item = new QListWidgetItem(output);
			QLabel* label = new QLabel(output+"\n"+furtherinfo);
		        QIcon icon = QIcon(":/AxIcons/people-woman-7_small@1x.png");
			icon.addPixmap(QPixmap(":/AxIcons/people-woman-7_small@1x.png"),QIcon::Selected);
			item->setIcon(icon);
			ui->spectrumChat->addItem(item);
			//ui->spectrumChat->setItemWidget(item, label);
                        if (!searchlist.contains(what) && !error) searchlist.prepend(what);
                        askme->setModel(new QStringListModel(searchlist, askme));
			//ui->furtherInfo->setText(furtherinfo);
                        //ui->spectrumEmoji->setIcon(QIcon(emoji));
                        //qDebug() << searchlist;
		    });*/
}

#include <Spectrum/O20Spectrum.h>

void O20Gui::getSpectrumAnswers()
{
   //if (plaintextmode && !alreadyPPS) addSpectrumChatEntry(O20AICON, "You seem to be editing a plain text file. In plain text mode, some of my features are disabled."), alreadyPPS = true;

   ui->spectrumChat->show();
   ui->welcome->hide();

   QString IPUT  = sender()->property("text").toString();
   QString OPUT = tr("Done!");
   addSpectrumChatEntry(noPhoto?"":usericon, IPUT);
   sender()->setProperty("text", "");

   QString INPUT = IPUT;
   QStringList LIST  = INPUT.split(' ', QString::SkipEmptyParts);

   int GOFLAG = 0, SELECTFLAG = 0, INSERTFLAG = 0;
   int GOLINE = 0, GONUMFLAG  = 0, GOWHERE = 0, GO_TO = 0, GOWORD = 0;
   int ITALIC = 0, BOLD = 0, UNDERLINE = 0, STRIKEOUT = 0, ALIGN = 0, ALIGNLEFT = 0, ALIGNRIGHT = 0, ALIGNCENTER = 0, ALIGNJUSTIFY = 0;
   int BULLET = 0, BULLETDISK = 0, BULLETCIRCLE = 0, BULLETSQUARE = 0, BULLETNUM = 0, BULLETALPHA = 0, BULLETLOWER = 0, BULLETROMAN = 0, BULLETROMANLOWER = 0;
   int STYLE = 0, UPDATESTYLE = 0, STYLENORMAL = 0, STYLEHEAD1 = 0, STYLEHEAD2 = 0, STYLEHEAD3 = 0, STYLEHEAD4 = 0, STYLEHEAD5 = 0, STYLETITLE = 0, STYLESUBTITLE = 0;
   int OVERLINE = 0, SUBSCRIPT = 0, SUPERSCRIPT = 0, CLEARFMT = 0, SELECT = 0;
   int SELECTLINE = 0, SELECTCHAR = 0, SELECTBLOCK = 0, SELECTALL = 0, RENAMEFLAG = 0;
   QString RENAMETO = "";

   QLocale::Language lang = QLocale::system().language();
   Spectrum::Engine p;
   if (lang == QLocale::Chinese) p.doChineseParse(LIST);
   else p.doEnglishParse(LIST); // english is the default


   /*
   for (QString i : LIST) {
       QString in = i;
       i = i.toLower();

       if (GOFLAG) {
           if (i == tr("to")) // as in go to line number 1
               continue;  // SKIP
	   else if (i == tr("up")) GOWHERE = 3;
	   else if (i == tr("down")) GOWHERE = 4;
	   else if (i == tr("line"))
               GOLINE = 1;
	   else if (i.contains(tr("begin")) or i == tr("start")) // as in beginning
               GOWHERE = 1; // to beginning of line 
           else if (i.contains(tr("end"))) // as in DocumentEnd (stoopid but true)
               GOWHERE = 2;
	   else if (i == tr("left") or i == tr("before") or i == tr("previous"))
               GOWHERE = 5;
	   else if (i.contains(tr("word")))
               GOWORD = 1;
	   else if (i == tr("right") or i == tr("next") or i == tr("after"))
               GOWHERE = 6;
	   else {
               bool isNum = false;
	       int num = i.toInt(&isNum);

	       if (isNum) {
	           GO_TO = num;
	           GONUMFLAG = 1;
	       }
	   }
       }

       if (RENAMEFLAG) RENAMETO += in;

       if (SELECTFLAG) {
           if (i == tr("line")) SELECTLINE = 1;
	   else if (i.contains(tr("char"))) SELECTCHAR = 1;
	   else if (i.contains(tr("para")) or i == tr("block")) SELECTBLOCK = 1;
	   else if (i == tr("all") or i.contains(tr("doc"))) SELECTALL = 1;
       }

       if (i == tr("go")) GOFLAG = 1;
       else if (i == tr("italic")) ITALIC = 1;
       else if (i == tr("bold")) BOLD = 1;
       else if (i == tr("underline")) UNDERLINE = 1;
       else if (i.contains(tr("strike"))) STRIKEOUT = 1;
       else if (i == tr("left")) ALIGNLEFT = 1;
       else if (i == tr("right")) ALIGNRIGHT = 1;
       else if (i == tr("center")) ALIGNCENTER = 1;
       else if (i == tr("justify")) ALIGNJUSTIFY = 1;
       else if (i == tr("bullet")) BULLETDISK = 1;
       else if (i == tr("number")) BULLETNUM = 1;
       else if (i == tr("select")) SELECTFLAG = 1;
       else if (i == tr("insert")) INSERTFLAG = 1;
       else if (i == tr("rename")) RENAMEFLAG = 1;
       else if (i == tr("overline") or i == tr("lineover")) OVERLINE = 1;
       else if (i.contains(tr("super"))) SUPERSCRIPT = 1;
       else if (i.contains(tr("sub"))) SUBSCRIPT = 1;
       else if (i == tr("clear")) CLEARFMT = 1;
   }*/


   QString ERRORMSG = tr("Something went wrong doing that.");
   bool ERROR = 0;
   ui->docView->setFocus();
   QTextCursor CUR = ui->docView->textCursor();
   if (p.saysomething != "") OPUT = p.saysomething;
   else if (p.GOFLAG) {
       ERRORMSG = tr("I can't take you there!");
       if (p.GOLINE) {
           if (p.GONUMFLAG) {
               if (p.GO_TO < 0) { // ?? Ugly, but what else do you think I can do?
                   ERROR = 1, ERRORMSG = tr("I don't do negatives..."); goto fin;
	       }

               ERROR = !CUR.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor, p.GO_TO);
	       if (p.GO_TO != 0)
	           ERROR = !CUR.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, p.GO_TO);
	       goto fin;
	   } else ERROR = 1;
       } else if (p.GOWHERE != 0) {
           if (p.GOWHERE == 1)
               ERROR = !CUR.movePosition(QTextCursor::Start);
	   else if (p.GOWHERE == 2)
               ERROR = !CUR.movePosition(QTextCursor::End);
	   else if (p.GOWHERE == 3)
               ERROR = !CUR.movePosition(QTextCursor::Up);
	   else if (p.GOWHERE == 4)
               ERROR = !CUR.movePosition(QTextCursor::Down);
           else if (p.GOWHERE == 6)
               ERROR = !CUR.movePosition(p.GOWORD ? QTextCursor::NextWord : QTextCursor::Left);
           else if (p.GOWHERE == 5)
               ERROR = !CUR.movePosition(p.GOWORD ? QTextCursor::PreviousWord : QTextCursor::Right);

       } else if (p.GONUMFLAG) {
           CUR.setPosition(p.GO_TO), ERRORMSG = tr("Sorry, I can't take you there."); goto fin;
       } else ERROR = 1, ERRORMSG = tr("Where do you want to go?");
   } else if (p.ITALIC) {
       ui->setItalic->click();
   } else if (p.BOLD) {
       ui->setBold->click();
   } else if (p.UNDERLINE) {
       ui->setUnderline->click();
   } else if (p.STRIKEOUT) {
       ui->setStrikeout->click();
   } else if (p.ALIGNLEFT) {
       ui->alignleft->click();
   } else if (p.ALIGNRIGHT) {
       ui->alignright->click();
   } else if (p.ALIGNCENTER) {
       ui->aligncenter->click();
   } else if (p.ALIGNJUSTIFY) {
       ui->alignjustify->click();
   } else if (p.OVERLINE) {
       ui->setOverline->click();
   } else if (p.SUBSCRIPT) {
       ui->setSubscript->click();
   } else if (p.SUPERSCRIPT) {
       ui->setSuperscript->click();
   } else if (p.CLEARFMT) {
       ui->eraseFormat->click();
   } else if (p.RENAMEFLAG) {
       if (QFileInfo(p.RENAMETO).suffix() == "" && document.suffix != "" && !plaintextmode) p.RENAMETO = p.RENAMETO + document.suffix;
       QFile(document.filename).rename(p.RENAMETO);
       document.filename = p.RENAMETO;
       document.suffix = QFileInfo(document.filename).suffix();
       setWindowTitle();
       AddRecentFile(true);
   } else if (p.SELECTFLAG) {
       QTextCursor::SelectionType op = QTextCursor::WordUnderCursor;

       if (p.SELECTCHAR) {
           CUR.movePosition(QTextCursor::Left, QTextCursor::KeepAnchor);
	   goto fin;
       } else if (p.SELECTLINE)
           op = QTextCursor::LineUnderCursor;
       else if (p.SELECTBLOCK)
           op = QTextCursor::BlockUnderCursor;
       else if (p.SELECTALL)
           op = QTextCursor::Document;
       else
           op = QTextCursor::WordUnderCursor;

       CUR.select(op);
   } else {
       ERROR = 1, ERRORMSG = tr("Could you try rephrasing that?");
   }

fin:

   if (!ERROR) ui->docView->setTextCursor(CUR);
   if (plaintextmode && !alreadyPPS && !ERROR) OPUT+=tr("\nBy the way, some of my features are disabled when editing plain text."), alreadyPPS = true;
   addSpectrumChatEntry(O20AICON, ERROR?ERRORMSG:OPUT);
   if (sender() == ui->spectrumInput) {
       sender()->setProperty("placeholderText", ERROR?ERRORMSG:OPUT);
   }
}

void O20Gui::addSpectrumChatEntry(QString icon, QString text)
{

    QListWidgetItem* item = new QListWidgetItem(text);
    QIcon icn = QIcon(icon);
    icn.addPixmap(QPixmap(icon), QIcon::Selected);
    item->setIcon(icn);
    ui->spectrumChat->addItem(item);
}
