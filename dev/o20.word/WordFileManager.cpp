#include "O20Gui.h"
#include "src/ui_O20Ui.h"

#include <QtWidgets>



void O20Gui::LoadFileManager(QString path) {

    QDir dir(path);

    if (!dir.exists() or !dir.isReadable()) return;

    dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoDot);
    dir.setSorting(QDir::Name | QDir::DirsFirst);

    ui->fileManager->clear();

    list = dir.entryInfoList();
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        QListWidgetItem* item = new QListWidgetItem(fileInfo.fileName());

        QString sfx = fileInfo.suffix();
        QString icnPath = "";
	if (fileInfo.isDir()) icnPath = ":/axialis/Basic/large/folder-vertical-open-filled.png";
	else if (sfx == "odt" or sfx == "ott" or sfx == "docx" or sfx == "dotx") icnPath = ":/axialis/Basic/large/document-text-picture.png";
        else if (sfx == "html" or sfx == "xhtml") icnPath = ":/axialis/Website and Email/large/page-html.png";
        else if (sfx == "md") icnPath = ":/axialis/Development/large/markup.png"; // markdown NOT markup (?)
        else if (sfx == "c") icnPath = ":/axialis/Development/large/file-type-c.png";
        else if (sfx == "cpp" or sfx == "cxx" or sfx == "c++" or sfx == "hpp") icnPath = ":/axialis/Development/large/file-type-cpp.png";
        else if (sfx == "h") icnPath = ":/axialis/Development/large/file-type-h.png";
        else if (sfx == "py") icnPath = ":/axialis/Development/large/script-filled.png";
        else icnPath = ":/axialis/Website and Email/large/page-code.png";

        QString dir = QFileInfo(path).absoluteDir().absolutePath();

        QIcon icon = QIcon(icnPath);
        icon.addPixmap(icnPath,QIcon::Selected);
        item->setIcon(icon);

        ui->fileManager->addItem(item);
    }

}
