import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts  1.12

import QtQuick.Controls.Material 2.12

import "Buttons" as O20BC
import "Other"   as O20OC

import "o20Scripting.js" as JS

//O20OC.DesktopScroll {
Rectangle {
    id: sidebar

    Material.accent: Qt.darker(themeColor, 0.5)
    Material.theme: Material.Dark

    property var sW: !documentEdit ? 120 : 120/2
    property var smallButtonWidth: sW

    property var buttonsRef: [homeButton, newButton, openButton, saveButton, aboutButton, optionsButton]

    width: sW
    height: 500;

    //background: Rectangle {
        color: themeColor
    //}

     ColumnLayout {
         id: sidebarColumnLayout
         anchors.fill: parent
         //height: sidebar.height

         spacing: 0

         O20BC.SidebarButton {
             id: goBack

            small: true
            checkable: false
            icon.source: "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/go-previous.svg"

            visible: documentEdit
            onClicked: JS.editDocument(), ribbonTabs.currentIndex = 1;
         }
        
         Rectangle {
              visible: !documentEdit
              height: 92
              Layout.fillWidth: true
              color: "transparent"

              Text {
                  anchors.fill: parent
                  font.pointSize: 14
                  font.family: "Ubuntu"

                  color: "white"
                  text: appName

                  horizontalAlignment: Text.AlignHCenter
                  verticalAlignment: Text.AlignVCenter
                  topPadding: 40
              }
         }


         O20BC.SidebarButton {
             id: homeButton

             small: documentEdit
             checked: true
             icon.source: "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/go-home.svg"
         }

         O20BC.SidebarButton {
             id: newButton
             small: documentEdit
             bz: "document-new-from-template"
         }

         O20BC.SidebarButton {
             id: openButton
             small: documentEdit
             bz: "document-open"
         }

         O20BC.SidebarButton {
             id: saveButton
             visible: documentEdit
             small: documentEdit
             bz: "document-save"
         }

         Item { Layout.fillHeight: true }


         O20BC.SidebarButton {
             id: aboutButton
             small: documentEdit
             implicitHeight: documentEdit ? sidebar.sW: sidebar.sW/2
             bz: "help-whatsthis"
         }

         O20BC.SidebarButton {
             id: optionsButton
             small: documentEdit
             implicitHeight: documentEdit ? sidebar.sW: sidebar.sW/2
             bz: "configure"
         }
    }
}
