import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import QtQuick.Controls.Universal 2.12
import QtQuick.Controls.Material 2.12


Pane {
    visible: true

    anchors.fill: parent

    //radius: 5

    width: 500
    height: 300

    Material.accent: appColor
    Material.theme: Material.Dark

    Universal.accent: "white"
    Universal.theme: Universal.Light

    background: Rectangle { radius: 5; color: Material.theme == Material.Light ? "#fafafa" : "#303030"}

    property string appColor : "#e83f24"
    property string appIcon : "../../../O20Core/apps/ms-office.svg" 
    property string qversion: "5.13.2"
    property string appName: "O20.App"

    //"../AppIcons/blackandwhite/WinWordLogoSmall.contrast-black_scale-180.png"

    //color: appColor

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        Rectangle {
             Layout.fillWidth: true
             height: 16
             color: "transparent"
        RowLayout {
           anchors.fill: parent
           Item { Layout.fillWidth: true; }
           ToolButton {
               icon.source: "../../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/window-close.svg"
               //icon.color: "#ffffff"
               height: 16
               width: 16
               padding: 2
               onClicked: {
                   Qt.quit();
               }
           } 
        }
        }

        Item { Layout.fillHeight: true }

        Rectangle {
           Layout.fillWidth: true

           height: 86
           color: "transparent"

           RowLayout {
           anchors.fill: parent

           Item { Layout.fillWidth: true; }

        Rectangle {
            height: 86
            width: 86
            //Layout.fillWidth: true
            color: "transparent"

            Image {
                anchors.centerIn: parent
                source: appIcon 
            }
        }

        Label {
            //Layout.fillWidth: true
            textFormat: Text.RichText
            text: appName
            //color: "white"
            font.pointSize: 24
            font.family: "Open Sans Light"
            horizontalAlignment: Text.AlignHCenter
        }

           Item { Layout.fillWidth: true; }

           /*ToolButton { 
               icon.source: "../BreezeDark/window-close.svg"; icon.color: "#ffffff"; 
               onClicked: {
                   Qt.quit()
               }
           }*/
           }
        }


        Label {
            Layout.fillWidth: true
            color: "white"
            
            text: qsTr("Built with " + qversion + ".")
            horizontalAlignment: Text.AlignHCenter
            font.family: "Open Sans"
            visible: false
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

            ProgressBar {
                visible: true
                //anchors.centerIn: parent
                Layout.alignment: Qt.AlignHCenter
                implicitWidth: 300
                indeterminate: true
            }

        Item { height: 10 }
    }
}

