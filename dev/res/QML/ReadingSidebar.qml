import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Rectangle {
   color: "transparent";
   Material.accent: Material.Blue
   Material.theme: Material.Light

   width: 42

   ColumnLayout {
       anchors.fill: parent
       ToolButton {
       }

       Item {
            Layout.fillHeight: true
       }
   }
}
