import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtQuick.Controls.Material 2.12

Rectangle {

    height: 40
    width: 60

    color: "#f5f5f5"
    visible: true;

    Material.accent: Material.Indigo
    Universal.theme: Universal.Light
    Universal.accent: Universal.Cyan

    Switch {
        anchors.fill: parent
        id: control
        objectName: "Switch"
        checked: true
        smooth: true
    }

    signal buttonClicked_enable()
}
