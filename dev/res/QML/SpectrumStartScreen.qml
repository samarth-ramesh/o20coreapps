import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12

Page {
    Universal.theme: Universal.Dark

    visible: true

    focusPolicy: Qt.ClickFocus

    ColumnLayout {
        Item {
            Layout.fillHeight: true
        }

        Item {height: 32}

/*
        AnimatedImage {
            Layout.alignment: Qt.AlignHCenter
            source: "CortanaAnimationJa.gif"
            speed: 0.5
        }  */

        Label {
            id: greeter
            objectName: "greetings"
            visible: false
            Layout.fillWidth: true
            font.pointSize: 24
            font.family: "Open Sans Light"
            text: "Hey there."
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: welcome
            Layout.fillWidth: true
            font.pointSize: 24 
            font.family: "Open Sans Light"
            text: "Office20"
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: loadText
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            text: "Word will be ready in just a moment..."
        }

        Rectangle {
            id: textRect
            visible: false
            Layout.alignment: Qt.AlignHCenter
            color: "transparent"
            width: 350
            height: 50

            TextField {
                objectName: "greeting"
                anchors.fill: parent 
                placeholderText: "Ask me anything"
                horizontalAlignment: Text.AlignHCenter

                onAccepted: {
                    focus = false
                }
            }
        }

        ToolButton {
            objectName: "backtoedit"
            visible: false
            icon.source: "../BreezeDark/go-previous.svg"
            icon.height: 16
            icon.width: 16
            icon.color: "transparent"

            Layout.alignment: Qt.AlignHCenter
            /*text: "Continue editing this document"*/
        }

        Item {
            Layout.fillHeight: true
        }

        Label {
            id: help
            visible: false
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Try saying 'open' or 'create'."
        }

        Item {
            Layout.fillHeight: true
        }

        CheckBox {
            id: switchb
            visible: false
            objectName: "Switch"
            Layout.alignment: Qt.AlignHCenter
            text: "Don't want to talk to me anymore?"
        }

        ProgressBar {
            id: progressBar
            Layout.fillWidth: true
            indeterminate: true
        }
    }

    /* changes colors slowly in background */
    background: Rectangle {
        color: "#e83f24"
        /*SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#217346"; duration: 20000 } // excel
            ColorAnimation { to: "#2b579a"; duration: 20000 } // word
            ColorAnimation { to: "#7719aa"; duration: 20000 } // onenote
            ColorAnimation { to: "#b7472a"; duration: 20000 } // powerpoint
            ColorAnimation { to: "#e83f24"; duration: 20000 } // office365
        }*/
    }

    /* Display widgets after splashscreen completes */
    function onInitDone() {
        switchb.visible = true
        help.visible = true
        textRect.visible = true
        greeter.visible = true

        welcome.visible = false 
        progressBar.visible = false
        loadText.visible = false
    }

    Component.onCompleted: { onInitDone(); }
}
